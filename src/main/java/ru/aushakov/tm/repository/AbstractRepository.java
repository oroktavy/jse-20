package ru.aushakov.tm.repository;

import ru.aushakov.tm.api.repository.IRepository;
import ru.aushakov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    public AbstractRepository() {
    }

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) {
        final List<E> entities = new ArrayList<>(list);
        entities.sort(comparator);
        return entities;
    }

    @Override
    public void add(final E entity) {
        list.add(entity);
    }

    @Override
    public void remove(final E entity) {
        list.remove(entity);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public E findOneById(final String id) {
        for (final E entity : list) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findOneByIndex(final Integer index) {
        return list.get(index);
    }

    @Override
    public E removeOneById(final String id) {
        final E entity = findOneById(id);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

    @Override
    public E removeOneByIndex(final Integer index) {
        final E entity = findOneByIndex(index);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

}
