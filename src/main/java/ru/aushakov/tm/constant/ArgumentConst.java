package ru.aushakov.tm.constant;

public interface ArgumentConst {

    String ARG_VERSION = "-v";

    String ARG_ABOUT = "-a";

    String ARG_HELP = "-h";

    String ARG_INFO = "-i";

}
