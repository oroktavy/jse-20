package ru.aushakov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.api.repository.IBusinessRepository;
import ru.aushakov.tm.api.service.IBusinessService;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.exception.empty.EmptyDescriptionException;
import ru.aushakov.tm.exception.empty.EmptyIdException;
import ru.aushakov.tm.exception.empty.EmptyNameException;
import ru.aushakov.tm.exception.general.InvalidIndexException;
import ru.aushakov.tm.exception.general.InvalidStatusException;
import ru.aushakov.tm.exception.general.NoUserLoggedInException;
import ru.aushakov.tm.model.AbstractBusinessEntity;
import ru.aushakov.tm.util.NumberUtil;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessService<E> {

    protected IBusinessRepository<E> businessRepository;

    protected Class<E> currentClass;

    public AbstractBusinessService() {
    }

    public AbstractBusinessService(IBusinessRepository<E> repository, Class<E> currentClass) {
        this.repository = repository;
        this.businessRepository = repository;
        this.currentClass = currentClass;
    }

    private E getNewEntity() {
        try {
            return currentClass.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<E> findAll(final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        return businessRepository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (comparator == null) return null;
        return businessRepository.findAll(userId, comparator);
    }

    @Override
    public E add(final String name, final String description, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        if (StringUtils.isEmpty(description)) throw new EmptyDescriptionException();
        final E entity = getNewEntity();
        entity.setUserId(userId);
        entity.setName(name);
        entity.setDescription(description);
        businessRepository.add(entity);
        return entity;
    }

    @Override
    public void clear(final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        businessRepository.clear(userId);
    }

    @Override
    public E findOneById(final String id, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return businessRepository.findOneById(id, userId);
    }

    @Override
    public E findOneByIndex(final Integer index, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return businessRepository.findOneByIndex(index, userId);
    }

    @Override
    public E findOneByName(final String name, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.findOneByName(name, userId);
    }

    @Override
    public E removeOneById(final String id, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return businessRepository.removeOneById(id, userId);
    }

    @Override
    public E removeOneByIndex(final Integer index, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return businessRepository.removeOneByIndex(index, userId);
    }

    @Override
    public E removeOneByName(final String name, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.removeOneByName(name, userId);
    }

    @Override
    public E updateOneById(
            final String id,
            final String userId,
            final String name,
            final String description
    ) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.updateOneById(id, userId, name, description);
    }

    @Override
    public E updateOneByIndex(
            final Integer index,
            final String userId,
            final String name,
            final String description
    ) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.updateOneByIndex(index, userId, name, description);
    }

    @Override
    public E startOneById(final String id, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return businessRepository.startOneById(id, userId);
    }

    @Override
    public E startOneByIndex(final Integer index, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return businessRepository.startOneByIndex(index, userId);
    }

    @Override
    public E startOneByName(final String name, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.startOneByName(name, userId);
    }

    @Override
    public E finishOneById(final String id, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return businessRepository.finishOneById(id, userId);
    }

    @Override
    public E finishOneByIndex(final Integer index, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return businessRepository.finishOneByIndex(index, userId);
    }

    @Override
    public E finishOneByName(final String name, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.finishOneByName(name, userId);
    }

    @Override
    public E changeOneStatusById(String id, Status status, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        return null;
    }

    @Override
    public E changeOneStatusByIndex(Integer index, Status status, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        return null;
    }

    @Override
    public E changeOneStatusByName(String name, Status status, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        return null;
    }

    @Override
    public E changeOneStatusById(final String id, final String statusId, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        final Status status = Status.toStatus(statusId);
        if (status == null) throw new InvalidStatusException(statusId);
        return businessRepository.changeOneStatusById(id, status, userId);
    }

    @Override
    public E changeOneStatusByIndex(final Integer index, final String statusId, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        final Status status = Status.toStatus(statusId);
        if (status == null) throw new InvalidStatusException(statusId);
        return businessRepository.changeOneStatusByIndex(index, status, userId);
    }

    @Override
    public E changeOneStatusByName(final String name, final String statusId, final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        final Status status = Status.toStatus(statusId);
        if (status == null) throw new InvalidStatusException(statusId);
        return businessRepository.changeOneStatusByName(name, status, userId);
    }

}
