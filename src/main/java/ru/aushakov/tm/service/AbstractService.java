package ru.aushakov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.api.repository.IRepository;
import ru.aushakov.tm.api.service.IService;
import ru.aushakov.tm.exception.empty.EmptyIdException;
import ru.aushakov.tm.exception.entity.NoEntityProvidedException;
import ru.aushakov.tm.exception.general.InvalidIndexException;
import ru.aushakov.tm.model.AbstractEntity;
import ru.aushakov.tm.util.NumberUtil;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected IRepository<E> repository;

    public AbstractService() {
    }

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) {
        if (comparator == null) return null;
        return repository.findAll(comparator);
    }

    @Override
    public void add(final E entity) {
        if (entity == null) throw new NoEntityProvidedException();
        repository.add(entity);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) throw new NoEntityProvidedException();
        repository.remove(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public E findOneById(final String id) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return repository.findOneById(id);
    }

    @Override
    public E findOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return repository.findOneByIndex(index);
    }

    @Override
    public E removeOneById(final String id) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return repository.removeOneById(id);
    }

    @Override
    public E removeOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return repository.removeOneByIndex(index);
    }

}
