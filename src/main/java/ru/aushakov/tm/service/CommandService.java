package ru.aushakov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.api.repository.ICommandRepository;
import ru.aushakov.tm.api.service.ICommandService;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.exception.empty.EmptyArgumentException;
import ru.aushakov.tm.exception.empty.EmptyCommandException;
import ru.aushakov.tm.exception.empty.EmptyNameException;

import java.util.Collection;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command.getName() == null) throw new EmptyNameException(command);
        commandRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByName(final String command) {
        if (StringUtils.isEmpty(command)) throw new EmptyCommandException();
        return commandRepository.getCommandByName(command);
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        if (StringUtils.isEmpty(arg)) throw new EmptyArgumentException();
        return commandRepository.getCommandByArg(arg);
    }

}
