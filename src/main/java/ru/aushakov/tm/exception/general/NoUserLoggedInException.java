package ru.aushakov.tm.exception.general;

public class NoUserLoggedInException extends RuntimeException {

    public NoUserLoggedInException() {
        super("No user's currently logged in!");
    }

}
