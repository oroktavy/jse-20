package ru.aushakov.tm.exception.general;

public class InvalidStatusException extends RuntimeException {

    public InvalidStatusException() {
        super("Status is invalid!");
    }

    public InvalidStatusException(final String statusId) {
        super("The value '" + statusId + "' entered is not a valid status!");
    }

}
