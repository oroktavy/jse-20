package ru.aushakov.tm.exception.general;

public class UnknownCommandException extends RuntimeException {

    public UnknownCommandException(final String command) {
        super("Command '" + command + "' is not supported");
    }

}
