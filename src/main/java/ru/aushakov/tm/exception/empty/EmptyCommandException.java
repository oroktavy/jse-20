package ru.aushakov.tm.exception.empty;

import ru.aushakov.tm.command.AbstractCommand;

public class EmptyCommandException extends RuntimeException {

    public EmptyCommandException() {
        super("No command provided!");
    }

    public EmptyCommandException(final AbstractCommand command) {
        super("Can not register/operate on empty command!");
    }

}
