package ru.aushakov.tm.model;

public class Task extends AbstractBusinessEntity {

    private String projectId;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

}
