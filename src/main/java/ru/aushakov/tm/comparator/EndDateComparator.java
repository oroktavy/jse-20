package ru.aushakov.tm.comparator;

import ru.aushakov.tm.api.entity.IHasEndDate;

import java.util.Comparator;
import java.util.Date;

public final class EndDateComparator implements Comparator<IHasEndDate> {

    private static final EndDateComparator INSTANCE = new EndDateComparator();

    private EndDateComparator() {
    }

    public static EndDateComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasEndDate o1, final IHasEndDate o2) {
        if (o1 == null || o2 == null) return ((o1 == null) ? ((o2 == null) ? 0 : -1) : 1);
        final Date endDate1 = o1.getEndDate();
        final Date endDate2 = o2.getEndDate();
        if (endDate1 == null || endDate2 == null) return ((endDate1 == null) ? ((endDate2 == null) ? 0 : -1) : 1);
        return endDate1.compareTo(endDate2);
    }

}
