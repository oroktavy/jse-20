package ru.aushakov.tm.util;

import ru.aushakov.tm.exception.general.InvalidIndexException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new InvalidIndexException(value);
        }
    }

}
