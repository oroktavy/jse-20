package ru.aushakov.tm.enumerated;

import ru.aushakov.tm.comparator.*;

import java.util.Comparator;

public enum SortType {

    NAME("Sort by name", NameComparator.getInstance()),
    STATUS("Sort by status", StatusComparator.getInstance()),
    CREATED("Sort by created date", CreatedComparator.getInstance()),
    START_DATE("Sort by start date", StartDateComparator.getInstance()),
    END_DATE("Sort by end date", EndDateComparator.getInstance());

    private final String displayName;

    private final Comparator comparator;

    private static final SortType[] staticValues = values();

    SortType(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public static SortType toSortType(final String sortTypeId) {
        for (final SortType sortType : staticValues) {
            if (sortType.name().equalsIgnoreCase(sortTypeId)) return sortType;
        }
        return null;
    }

}
