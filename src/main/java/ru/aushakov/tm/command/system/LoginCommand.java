package ru.aushakov.tm.command.system;

import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.TerminalConst;

public class LoginCommand extends AbstractCommand {

    @Override
    public String getName() {
        return TerminalConst.CMD_LOGIN;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Makes you able to login twice";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("OK now, you're fully logged in, but you can try once more");
    }

}
