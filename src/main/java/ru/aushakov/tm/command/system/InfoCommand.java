package ru.aushakov.tm.command.system;

import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.ArgumentConst;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.util.NumberUtil;

public class InfoCommand extends AbstractCommand {

    @Override
    public String getName() {
        return TerminalConst.CMD_INFO;
    }

    @Override
    public String getArgument() {
        return ArgumentConst.ARG_INFO;
    }

    @Override
    public String getDescription() {
        return "Show system information";
    }

    @Override
    public void execute() {
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("[SYSTEM INFO]");
        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory (bytes): " + NumberUtil.bytesToText(freeMemory));
        System.out.println("Maximum memory (bytes): " +
                (maxMemory == Long.MAX_VALUE ? "no limit" : NumberUtil.bytesToText(maxMemory)));
        System.out.println("Total memory available to JVM (bytes): " +
                NumberUtil.bytesToText(totalMemory));
        System.out.println("Used memory by JVM (bytes): " + NumberUtil.bytesToText(usedMemory));
    }

}
