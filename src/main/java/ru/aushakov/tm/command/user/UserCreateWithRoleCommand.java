package ru.aushakov.tm.command.user;

import ru.aushakov.tm.command.AbstractUserCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.Role;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Arrays;

public class UserCreateWithRoleCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return TerminalConst.USER_CREATE_WITH_ROLE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Create user with role";
    }

    @Override
    public void execute() {
        System.out.println("[USER CREATE]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL (OPTIONAL):");
        final String email = TerminalUtil.nextLine();
        System.out.println("ENTER ROLE " + Arrays.toString(Role.values()) + " :");
        final String roleId = TerminalUtil.nextLine();
        serviceLocator.getUserService().add(login, password, email, roleId);
    }

}
