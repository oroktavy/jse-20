package ru.aushakov.tm.command.user;

import ru.aushakov.tm.command.AbstractUserCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.entity.UserNotFoundException;
import ru.aushakov.tm.model.User;
import ru.aushakov.tm.util.TerminalUtil;

public class UserUpdateByLoginCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return TerminalConst.USER_UPDATE_BY_LOGIN;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Update user by login";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER]");
        System.out.println("ENTER USER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().updateOneByLogin(login, lastName, firstName, middleName);
        if (user == null) throw new UserNotFoundException();
    }

}
