package ru.aushakov.tm.command.task;

import ru.aushakov.tm.command.AbstractTaskCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.exception.entity.TaskNotFoundException;
import ru.aushakov.tm.model.Task;
import ru.aushakov.tm.util.TerminalUtil;

public class TaskFinishByIdCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_FINISH_BY_ID;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Finish task by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishOneById(id, userId);
        if (task == null) throw new TaskNotFoundException();
    }

}
